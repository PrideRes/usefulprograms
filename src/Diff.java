/*
Author : Wan Li Hau 
Purpose: To find out the difference between 2 cols in CSV files which is pretty frustrating. 
*/
import java.util.*; 
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Diff
{
	public static void main(String[] args)
	{
		if(args.length ==0)
		{
			System.out.println("Please run the program in the following manner : \"DiffCol <filename>\"");
			return;
		}
		
		
		if(args[0].equals("-help"))
		{
			System.out.println("1) Run command DiffCol <filename>.");
			System.out.println("2) File should consist of 2 column, seperated by comma.");
			System.out.println("3) your column values should not contain \",\"");
			System.out.println("4) Column 2 should be longer than Column 1 or = to Column 1.");
			System.out.println("5) Program does not consider in the event your values in the column have ,");
			return;
		}
		
		String filename = args[0];
		
		List<String> col1 = new LinkedList<String>(); 
		List<String> col2 = new LinkedList<String>();
		String seperator = args.length == 1 ? ",":args[1];
			
		BufferedReader br = null;
		
		try
		{
			br = new BufferedReader(new FileReader(filename));
			String line = "" ;
			while((line=br.readLine())!=null)
			{
				String values[] = line.split(seperator);
				if(!values[0].isEmpty())
					col1.add(values[0]);
				if(!values[1].isEmpty())
					col2.add(values[1]);
			}
			
			Iterator it1 = col1.iterator();
			Iterator it2 = col2.iterator(); 
			
			while(it1.hasNext())
			{
				String col1Val = (String) it1.next();
				it2=col2.iterator();
				while(it2.hasNext())
				{
					String col2Val = (String) it2.next(); 
					if(col1Val.equals(col2Val)) 
					{
						it1.remove();
						it2.remove(); 
						break;
					}
				}
			}
			
			it1 = col1.iterator(); 
			it2 = col2.iterator();
			
			System.out.println("this is what is in col1 and not in col2");
			while(it1.hasNext())
			{
				System.out.println(it1.next());
			}
			
			System.out.println("\n\nthis is what is in col2 and not in col1");
			while(it2.hasNext())
			{
				System.out.println(it2.next());
			}
			//test adding a random comment.
			
		}catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("If you are seeing this message, it either means you've came up with some weird scenario,"); 
			System.out.println("OR you dont know how to use the program. Please run DiffCol -help");
		}
		finally
		{
			if(br!= null)
			{
				try{
					br.close();
				}catch(IOException ie)
				{
					ie.printStackTrace(); 
				}
				
			}
		}
	}
}